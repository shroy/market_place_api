Rails.application.routes.draw do
  namespace :api, defaults: { formats: :json },
                  contraints: { subdomain: :api }, path: '/' do

    scope module: :v1,
      constraints: ApiConstraints.new(version: 1, default: true) do

    end
  end
end
